@extends('templates.appTemplate')

@section('content')

    <div class="row justify-content-md-center">
        <div class="col-md-8">
            <h1 class="mt-5"><strong> {{ $title }} </strong></h1>
            <hr>
            
            <form method="post" action="{{ route('webhook') }}">
            {{csrf_field()}}
            
                <div class="form-group">
                    <label>Name: </label>
                    <input type="text" name="user_name" class="form-control">

                    @if ($errors -> has('user_name'))
                        <small class="form-text invalid-feedback">{{ $errors -> first('user_name') }}</small>
                    @endif
                </div>  

                <div class="form-group">
                    <label>E-mail: </label>
                    <input type="email" name="user_email" class="form-control">

                    @if ($errors -> has('user_email'))
                        <small class="form-text invalid-feedback">{{ $errors -> first('user_email') }}</small>
                    @endif
                </div>  

                <div class="form-group">
                    <label>URL: </label>
                    <input type="text" name="callbackurl" class="form-control">

                    @if ($errors -> has('callbackurl'))
                        <small class="form-text invalid-feedback">{{ $errors -> first('callbackurl') }}</small>
                    @endif
                </div>  

                <button class="btn btn-primary">Submit</button>                    
            </form>
        </div>
    </div>

@endsection